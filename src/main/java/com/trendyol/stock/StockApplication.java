package com.trendyol.stock;

//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;


@SpringBootApplication
@RestController
public class StockApplication {

    @GetMapping("/")
	String home() {
		return "Spring is here!";
	}


    public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }

}
